/*
 * main.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_io.h"
#include "xparameters.h"
#include "MTF_IP.h"


int code_MTF(u8 letter, u32 *key);
int decode_MTF(u8 key, u32 *letter);

u32 read2DigitDecVal(){
	u32 ret = 0;
	char8 c;
	outbyte ( c = inbyte() );
	ret += 10 * (c - '0');
	outbyte ( c = inbyte() );
	ret += (c - '0');
	return ret;
}

int main()
{
	u8 letter;
	u32 key;

	init_platform();

    while(1){
    	print("Enter char (A-Z):\r");
    	letter = inbyte();
    	if (letter >= 'A' && letter <= 'Z' ){
    		code_MTF(letter, &key);
    		xil_printf("%d\r", key );
    	} else if (letter >= '0' && letter <= '9' )
    	{
    		decode_MTF(letter-'0', &key);
    		xil_printf("%d\r", key );
    	}
    }
}
