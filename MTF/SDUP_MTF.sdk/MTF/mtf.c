/***************************** Include Files *********************************/
#include "xil_io.h"
#include "xparameters.h"
#include "MTF_IP.h"

/**************************** user definitions ********************************/

//MTF processor base addres redefinition
#define MTF_BASE_ADDR      XPAR_MTF_IP_0_S00_AXI_BASEADDR
//MTF processor registers' offset redefinition
#define CONTROL_REG_OFFSET    MTF_IP_S00_AXI_SLV_REG0_OFFSET
#define INPUT_REG_OFFSET      MTF_IP_S00_AXI_SLV_REG1_OFFSET
#define STATUS_REG_OFFSET     MTF_IP_S00_AXI_SLV_REG2_OFFSET
#define RESULT_REG_OFFSET    MTF_IP_S00_AXI_SLV_REG3_OFFSET
//MTF processor bits masks
#define CONTROL_REG_START_CODE_MASK (u32)(0x01)
#define STATUS_REG_READY_CODE_MASK (u32)(0x01)

#define CONTROL_REG_START_DECODE_MASK (u32)(0x55)
#define STATUS_REG_READY_DECODE_MASK (u32)(0x02)



int code_MTF(u8 letter, u32 *key)
{
	u32 result = 0;

	u32 data = (letter << 1) | 1;


	MTF_IP_mWriteReg(MTF_BASE_ADDR, INPUT_REG_OFFSET, data);

	//Start MTF processor - pulse start bit in control register

	MTF_IP_mWriteReg(MTF_BASE_ADDR, CONTROL_REG_OFFSET, CONTROL_REG_START_CODE_MASK);
	MTF_IP_mWriteReg(MTF_BASE_ADDR, CONTROL_REG_OFFSET, 0);
	
	//Wait for ready bit in status register
	while( (MTF_IP_mReadReg(MTF_BASE_ADDR, STATUS_REG_OFFSET) & STATUS_REG_READY_CODE_MASK) == 0);

	result = MTF_IP_mReadReg(MTF_BASE_ADDR, RESULT_REG_OFFSET);
	*key = result&0xFFFF;

	return 1;
}


int decode_MTF(u8 key, u32 *letter)
{
	u32 result = 0;

	u32 data = (key << 1);


	MTF_IP_mWriteReg(MTF_BASE_ADDR, INPUT_REG_OFFSET, data);

	//Start MTF processor - pulse start bit in control register

	MTF_IP_mWriteReg(MTF_BASE_ADDR, CONTROL_REG_OFFSET, CONTROL_REG_START_DECODE_MASK);
	MTF_IP_mWriteReg(MTF_BASE_ADDR, CONTROL_REG_OFFSET, 0);

	//Wait for ready bit in status register
	while( (MTF_IP_mReadReg(MTF_BASE_ADDR, STATUS_REG_OFFSET) & STATUS_REG_READY_CODE_MASK) == 0);

	result = MTF_IP_mReadReg(MTF_BASE_ADDR, RESULT_REG_OFFSET);
	*letter = result>>16;

	return 1;
}
