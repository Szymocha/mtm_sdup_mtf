`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.08.2021 14:50:00
// Design Name: 
// Module Name: MTF_decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MTF_decoder(
    input wire [ 4 : 0 ] number,
    output reg [ 7 : 0 ] key,
    
    input wire start,
    output reg done,
    
    input wire clk,
    input wire rst
    );
    
  // parameter DICT_LENGTH = 26;
  // DICT_LENGTH * 8 - 1
  
  reg [ 207 : 0 ] dict = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  reg [ 7 : 0 ] key0 = 0, key1 = 0, key2 = 0, key3 = 0, key4 = 0, key5 = 0, key6 = 0, key7 = 0, key8 = 0, key9 = 0, key10 = 0, key11 = 0, key12 = 0, key13 = 0, key14 = 0, key15 = 0, key16 = 0, key17 = 0, key18 = 0, key19 = 0, key20 = 0, key21 = 0, key22 = 0, key23 = 0, key24 = 0;
  
  always @* begin
    if(rst)begin
      dict = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key0 <= 0;
    end else if(start)begin
      if( number == 26 )begin
        key0 <= dict[ 7 : 0 ];
      end else begin
        key0 <= number;
      end
    end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key1 <= 0;
    end else if( key0 < "A" )begin
        if( key0 == 25 )begin
          key1 <= dict[ 15 : 8 ];
        end else begin
          key1 <= key0;
        end
      end else begin
        dict[ 7 : 0 ] <= dict[ 15 : 8 ];
        key1 <= key0;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key2 <= 0;
    end else if( key1 < "A" )begin
        if( key1 == 24 )begin
          key2 <= dict[ 23 : 16 ];
        end else begin
          key2 <= key1;
        end
      end else begin
        dict[ 15 : 8 ] <= dict[ 23 : 16 ];
        key2 <= key1;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key3 <= 0;
    end else if( key2 < "A" )begin
        if( key2 == 23 )begin
          key3 <= dict[ 31 : 24 ];
        end else begin
          key3 <= key2;
        end
      end else begin
        dict[ 23 : 16 ] <= dict[ 31 : 24 ];
        key3 <= key2;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key4 <= 0;
    end else if( key3 < "A" )begin
        if( key3 == 22 )begin
          key4 <= dict[ 39 : 32 ];
        end else begin
          key4 <= key3;
        end
      end else begin
        dict[ 31 : 24 ] <= dict[ 39 : 32 ];
        key4 <= key3;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key5 <= 0;
    end else if( key4 < "A" )begin
        if( key4 == 21 )begin
          key5 <= dict[ 47 : 40 ];
        end else begin
          key5 <= key4;
        end
      end else begin
        dict[ 39 : 32 ] <= dict[ 47 : 40 ];
        key5 <= key4;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key6 <= 0;
    end else if( key5 < "A" )begin
        if( key5 == 20 )begin
          key6 <= dict[ 55 : 48 ];
        end else begin
          key6 <= key5;
        end
      end else begin
        dict[ 47 : 40 ] <= dict[ 55 : 48 ];
        key6 <= key5;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key7 <= 0;
    end else if( key6 < "A" )begin
        if( key6 == 19 )begin
          key7 <= dict[ 63 : 56 ];
        end else begin
          key7 <= key6;
        end
      end else begin
        dict[ 55 : 48 ] <= dict[ 63 : 56 ];
        key7 <= key6;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key8 <= 0;
    end else if( key7 < "A" )begin
        if( key7 == 18 )begin
          key8 <= dict[ 71 : 64 ];
        end else begin
          key8 <= key7;
        end
      end else begin
        dict[ 63 : 56 ] <= dict[ 71 : 64 ];
        key8 <= key7;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key9 <= 0;
    end else if( key8 < "A" )begin
        if( key8 == 17 )begin
          key9 <= dict[ 79 : 72 ];
        end else begin
          key9 <= key8;
        end
      end else begin
        dict[ 71 : 64 ] <= dict[ 79 : 72 ];
        key9 <= key8;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key10 <= 0;
    end else if( key9 < "A" )begin
        if( key9 == 16 )begin
          key10 <= dict[ 87 : 80 ];
        end else begin
          key10 <= key9;
        end
      end else begin
        dict[ 79 : 72 ] <= dict[ 87 : 80 ];
        key10 <= key9;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key11 <= 0;
    end else if( key10 < "A" )begin
        if( key10 == 15 )begin
          key11 <= dict[ 95 : 88 ];
        end else begin
          key11 <= key10;
        end
      end else begin
        dict[ 87 : 80 ] <= dict[ 95 : 88 ];
        key11 <= key10;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key12 <= 0;
    end else if( key11 < "A" )begin
        if( key11 == 14 )begin
          key12 <= dict[ 103 : 96 ];
        end else begin
          key12 <= key11;
        end
      end else begin
        dict[ 95 : 88 ] <= dict[ 103 : 96 ];
        key12 <= key11;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key13 <= 0;
    end else if( key12 < "A" )begin
        if( key12 == 13 )begin
          key13 <= dict[ 111 : 104 ];
        end else begin
          key13 <= key12;
        end
      end else begin
        dict[ 103 : 96 ] <= dict[ 111 : 104 ];
        key13 <= key12;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key14 <= 0;
    end else if( key13 < "A" )begin
        if( key13 == 12 )begin
          key14 <= dict[ 119 : 112 ];
        end else begin
          key14 <= key13;
        end
      end else begin
        dict[ 111 : 104 ] <= dict[ 119 : 112 ];
        key14 <= key13;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key15 <= 0;
    end else if( key14 < "A" )begin
        if( key14 == 11 )begin
          key15 <= dict[ 127 : 120 ];
        end else begin
          key15 <= key14;
        end
      end else begin
        dict[ 119 : 112 ] <= dict[ 127 : 120 ];
        key15 <= key14;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key16 <= 0;
    end else if( key15 < "A" )begin
        if( key15 == 10 )begin
          key16 <= dict[ 135 : 128 ];
        end else begin
          key16 <= key15;
        end
      end else begin
        dict[ 127 : 120 ] <= dict[ 135 : 128 ];
        key16 <= key15;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key17 <= 0;
    end else if( key16 < "A" )begin
        if( key16 == 9 )begin
          key17 <= dict[ 143 : 136 ];
        end else begin
          key17 <= key16;
        end
      end else begin
        dict[ 135 : 128 ] <= dict[ 143 : 136 ];
        key17 <= key16;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key18 <= 0;
    end else if( key17 < "A" )begin
        if( key17 == 8 )begin
          key18 <= dict[ 151 : 144 ];
        end else begin
          key18 <= key17;
        end
      end else begin
        dict[ 143 : 136 ] <= dict[ 151 : 144 ];
        key18 <= key17;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key19 <= 0;
    end else if( key18 < "A" )begin
        if( key18 == 7 )begin
          key19 <= dict[ 159 : 152 ];
        end else begin
          key19 <= key18;
        end
      end else begin
        dict[ 151 : 144 ] <= dict[ 159 : 152 ];
        key19 <= key18;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key20 <= 0;
    end else if( key19 < "A" )begin
        if( key19 == 6 )begin
          key20 <= dict[ 167 : 160 ];
        end else begin
          key20 <= key19;
        end
      end else begin
        dict[ 159 : 152 ] <= dict[ 167 : 160 ];
        key20 <= key19;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key21 <= 0;
    end else if( key20 < "A" )begin
        if( key20 == 5 )begin
          key21 <= dict[ 175 : 168 ];
        end else begin
          key21 <= key20;
        end
      end else begin
        dict[ 167 : 160 ] <= dict[ 175 : 168 ];
        key21 <= key20;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key22 <= 0;
    end else if( key21 < "A" )begin
        if( key21 == 4 )begin
          key22 <= dict[ 183 : 176 ];
        end else begin
          key22 <= key21;
        end
      end else begin
        dict[ 175 : 168 ] <= dict[ 183 : 176 ];
        key22 <= key21;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key23 <= 0;
    end else if( key22 < "A" )begin
        if( key22 == 3 )begin
          key23 <= dict[ 191 : 184 ];
        end else begin
          key23 <= key22;
        end
      end else begin
        dict[ 183 : 176 ] <= dict[ 191 : 184 ];
        key23 <= key22;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key24 <= 0;
    end else if( key23 < "A" )begin
        if( key23 == 2 )begin
          key24 <= dict[ 199 : 192 ];
        end else begin
          key24 <= key23;
        end
      end else begin
        dict[ 191 : 184 ] <= dict[ 199 : 192 ];
        key24 <= key23;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key <= 0;
    end else if( key24 < "A" )begin
        if( key24 == 1 )begin
          key <= dict[ 207 : 200 ];
        end else begin
          key <= 30;
        end
      end else begin
        dict[ 199 : 192 ] <= dict[ 207 : 200 ];
        dict[ 207 : 200 ] <= key24;
        key <= key24;
      end
  end
  
  always @(posedge clk)begin
    if( key24 > 0 )begin
      done <= 1;
    end else begin
      done <= 0;
    end
  end
  
endmodule
