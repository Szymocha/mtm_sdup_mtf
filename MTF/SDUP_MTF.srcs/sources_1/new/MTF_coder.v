`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.08.2021 14:50:00
// Design Name: 
// Module Name: MTF_coder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MTF_coder (
    input wire [ 7 : 0 ] letter,
    output reg [ 4 : 0 ] key,
    
    input wire start,
    output reg done,
    
    input wire clk,
    input wire rst
    );
    
  // parameter DICT_LENGTH = 26;
  // DICT_LENGTH * 8 - 1
  
  reg [ 207 : 0 ] dict = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  reg [ 7 : 0 ] tmp0, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9, tmp10, tmp11, tmp12, tmp13, tmp14, tmp15, tmp16, tmp17, tmp18, tmp19, tmp20, tmp21, tmp22, tmp23, tmp24;
  reg [ 7 : 0 ] key0 = 0, key1 = 0, key2 = 0, key3 = 0, key4 = 0, key5 = 0, key6 = 0, key7 = 0, key8 = 0, key9 = 0, key10 = 0, key11 = 0, key12 = 0, key13 = 0, key14 = 0, key15 = 0, key16 = 0, key17 = 0, key18 = 0, key19 = 0, key20 = 0, key21 = 0, key22 = 0, key23 = 0, key24 = 0;
  
  always @* begin
    if(rst)begin
      dict = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    end
  end
    
  always @(posedge clk) begin
    if(rst)begin
      key0 <= 0;
    end else if(start)begin
      if( letter == dict[ 207 : 200 ] )begin
        key0 <= 1;
      end else begin
        tmp0 <= dict[ 207 : 200 ];
        key0 <= letter;
      end
      dict[ 207 : 200 ] <= letter;
    end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key1 <= 0;
    end else if( key0 < "A" )begin
        key1 <= key0;
      end else begin
        if( key0 == dict[ 199 : 192 ] )begin
          key1 <= 2;
        end else begin
          tmp1 <= dict[ 199 : 192 ];
          key1 <= key0;
        end
        dict[ 199 : 192 ] <= tmp0;
    end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key2 <= 0;
    end else if( key1 < "A" )begin
        key2 <= key1;
      end else begin
        if( key1 == dict[ 191 : 184 ] )begin
          key2 <= 3;
        end else begin
          tmp2 <= dict[ 191 : 184 ];
          key2 <= key1;
        end
        dict[ 191 : 184 ] <= tmp1;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key3 <= 0;
    end else if( key2 < "A" )begin
        key3 <= key2;
      end else begin
        if( key2 == dict[ 183 : 176 ] )begin
          key3 <= 4;
        end else begin
          tmp3 <= dict[ 183 : 176 ];
          key3 <= key2;
        end
        dict[ 183 : 176 ] <= tmp2;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key4 <= 0;
    end else if( key3 < "A" )begin
        key4 <= key3;
      end else begin
        if( key3 == dict[ 175 : 168 ] )begin
          key4 <= 5;
        end else begin
          tmp4 <= dict[ 175 : 168 ];
          key4 <= key3;
        end
        dict[ 175 : 168 ] <= tmp3;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key5 <= 0;
    end else if( key4 < "A" )begin
        key5 <= key4;
      end else begin
        if( key4 == dict[ 167 : 160 ] )begin
          key5 <= 6;
        end else begin
          tmp5 <= dict[ 167 : 160 ];
          key5 <= key4;
        end
        dict[ 167 : 160 ] <= tmp4;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key6 <= 0;
    end else if( key5 < "A" )begin
        key6 <= key5;
      end else begin
        if( key5 == dict[ 159 : 152 ] )begin
          key6 <= 7;
        end else begin
          tmp6 <= dict[ 159 : 152 ];
          key6 <= key5;
        end
        dict[ 159 : 152 ] <= tmp5;
    end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key7 <= 0;
    end else if( key6 < "A" )begin
        key7 <= key6;
      end else begin
        if( key6 == dict[ 151 : 144 ] )begin
          key7 <= 8;
        end else begin
          tmp7 <= dict[ 151 : 144 ];
          key7 <= key6;
        end
        dict[ 151 : 144 ] <= tmp6;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key8 <= 0;
    end else if( key7 < "A" )begin
        key8 <= key7;
      end else begin
        if( key7 == dict[ 143 : 136 ] )begin
          key8 <= 9;
        end else begin
          tmp8 <= dict[ 143 : 136 ];
          key8 <= key7;
        end
        dict[ 143 : 136 ] <= tmp7;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key9 <= 0;
    end else if( key8 < "A" )begin
        key9 <= key8;
      end else begin
        if( key8 == dict[ 135 : 128 ] )begin
          key9 <= 10;
        end else begin
          tmp9 <= dict[ 135 : 128 ];
          key9 <= key8;
        end
        dict[ 135 : 128 ] <= tmp8;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key10 <= 0;
    end else if( key9 < "A" )begin
        key10 <= key9;
      end else begin
        if( key9 == dict[ 127 : 120 ] )begin
          key10 <= 11;
        end else begin
          tmp10 <= dict[ 127 : 120 ];
          key10 <= key9;
        end
        dict[ 127 : 120 ] <= tmp9;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key11 <= 0;
    end else if( key10 < "A" )begin
        key11 <= key10;
      end else begin
        if( key10 == dict[ 119 : 112 ] )begin
          key11 <= 12;
        end else begin
          tmp11 <= dict[ 119 : 112 ];
          key11 <= key10;
        end
        dict[ 119 : 112 ] <= tmp10;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key12 <= 0;
    end else if( key11 < "A" )begin
        key12 <= key11;
      end else begin
        if( key11 == dict[ 111 : 104 ] )begin
          key12 <= 13;
        end else begin
          tmp12 <= dict[ 111 : 104 ];
          key12 <= key11;
        end
        dict[ 111 : 104 ] <= tmp11;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key13 <= 0;
    end else if( key12 < "A" )begin
        key13 <= key12;
      end else begin
        if( key12 == dict[ 103 : 96 ] )begin
          key13 <= 14;
        end else begin
          tmp13 <= dict[ 103 : 96 ];
          key13 <= key12;
        end
        dict[ 103 : 96 ] <= tmp12;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key14 <= 0;
    end else if( key13 < "A" )begin
        key14 <= key13;
      end else begin
        if( key13 == dict[ 95 : 88 ] )begin
          key14 <= 15;
        end else begin
          tmp14 <= dict[ 95 : 88 ];
          key14 <= key13;
        end
        dict[ 95 : 88 ] <= tmp13;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key15 <= 0;
    end else if( key14 < "A" )begin
        key15 <= key14;
      end else begin
        if( key14 == dict[ 87 : 80 ] )begin
          key15 <= 16;
        end else begin
          tmp15 <= dict[ 87 : 80 ];
          key15 <= key14;
        end
        dict[ 87 : 80 ] <= tmp14;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key16 <= 0;
    end else if( key15 < "A" )begin
        key16 <= key15;
      end else begin
        if( key15 == dict[ 79 : 72 ] )begin
          key16 <= 17;
        end else begin
          tmp16 <= dict[ 79 : 72 ];
          key16 <= key15;
        end
        dict[ 79 : 72 ] <= tmp15;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key17 <= 0;
    end else if( key16 < "A" )begin
        key17 <= key16;
      end else begin
        if( key16 == dict[ 71 : 64 ] )begin
          key17 <= 18;
        end else begin
          tmp17 <= dict[ 71 : 64 ];
          key17 <= key16;
        end
        dict[ 71 : 64 ] <= tmp16;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key18 <= 0;
    end else if( key17 < "A" )begin
        key18 <= key17;
      end else begin
        if( key17 == dict[ 63 : 56 ] )begin
          key18 <= 19;
        end else begin
          tmp18 <= dict[ 63 : 56 ];
          key18 <= key17;
        end
        dict[ 63 : 56 ] <= tmp17;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key19 <= 0;
    end else if( key18 < "A" )begin
        key19 <= key18;
      end else begin
        if( key18 == dict[ 55 : 48 ] )begin
          key19 <= 20;
        end else begin
          tmp19 <= dict[ 55 : 48 ];
          key19 <= key18;
        end
        dict[ 55 : 48 ] <= tmp18;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key20 <= 0;
    end else if( key19 < "A" )begin
        key20 <= key19;
      end else begin
        if( key19 == dict[ 47 : 40 ] )begin
          key20 <= 21;
        end else begin
          tmp20 <= dict[ 47 : 40 ];
          key20 <= key19;
        end
        dict[ 47 : 40 ] <= tmp19;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key21 <= 0;
    end else if( key20 < "A" )begin
        key21 <= key20;
      end else begin
        if( key20 == dict[ 39 : 32 ] )begin
          key21 <= 22;
        end else begin
          tmp21 <= dict[ 39 : 32 ];
          key21 <= key20;
        end
        dict[ 39 : 32 ] <= tmp20;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key22 <= 0;
    end else if( key21 < "A" )begin
        key22 <= key21;
      end else begin
        if( key21 == dict[ 31 : 24 ] )begin
          key22 <= 23;
        end else begin
          tmp22 <= dict[ 31 : 24 ];
          key22 <= key21;
        end
        dict[ 31 : 24 ] <= tmp21;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key23 <= 0;
    end else if( key22 < "A" )begin
        key23 <= key22;
      end else begin
        if( key22 == dict[ 23 : 16 ] )begin
          key23 <= 24;
        end else begin
          tmp23 <= dict[ 23 : 16 ];
          key23 <= key22;
        end
        dict[ 23 : 16 ] <= tmp22;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key24 <= 0;
    end else if( key23 < "A" )begin
        key24 <= key23;
      end else begin
        if( key23 == dict[ 15 : 8 ] )begin
          key24 <= 25;
        end else begin
          tmp24 <= dict[ 15 : 8 ];
          key24 <= key23;
        end
        dict[ 15 : 8 ] <= tmp23;
      end
  end
  
  always @(posedge clk) begin
    if(rst)begin
      key <= 0;
    end else if( key24 < "A" )begin
        key <= key24;
      end else begin
        if( key24 == dict[ 7 : 0 ] )begin
          key <= 26;
        end else begin
          key <= 30;
        end
        dict[ 7 : 0 ] <= tmp24;
      end
  end
  
  always @(posedge clk)begin
    if( key24 > 0 )begin
      done <= 1;
    end else begin
      done <= 0;
    end
  end
  
endmodule
