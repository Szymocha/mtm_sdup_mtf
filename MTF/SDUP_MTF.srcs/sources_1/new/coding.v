`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.08.2021 15:48:14
// Design Name: 
// Module Name: coding
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module coding (
    input wire [ 7 : 0 ] iteration,
    input wire [ 7 : 0 ] key_in,
    input wire [ 7 : 0 ] tmp_in,
    input wire [ 7 : 0 ] dict_in,
    
    output reg [ 7 : 0 ] key_out,
    output reg [ 7 : 0 ] tmp_out,
    output reg [ 7 : 0 ] dict_out,
    
    input wire clk
    );
    
  always @(posedge clk) begin
    if( key_in < "A" )begin
      key_out <= key_in;
    end else begin
      if( key_in == dict_in )begin
        key_out <= iteration;
      end else begin
        tmp_out <= dict_in;
        key_out <= key_in;
      end
      dict_out <= tmp_in;
    end
  end
  
endmodule
