`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.08.2021 15:19:08
// Design Name: 
// Module Name: MTF_coder_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MTF_coder_TB(

    );
    
//    wire [ 4 : 0 ] key;
    reg [ 7 : 0 ] letter;
    reg clk, rst, start;
    reg [31:0] slv_reg1, slv_reg2, slv_reg3, slv_reg0;
    wire [31:0] slv_wire2, slv_wire3;
//    MTF_coder coder_test( letter, key, start, done, clk, rst );
    
    reg code_start, decode_start;

    
    always @* begin
      code_start = slv_reg0[7:0] == 8'hAA;
      decode_start = slv_reg0[7:0] == 8'h55;
    end
    
    always @( posedge clk )
    begin
     slv_reg2 <= slv_wire2;
     slv_reg3 <= slv_wire3;
    end
    
    //Assign zeros to unused bits
    assign slv_wire2[31:2] = 30'b0;
    assign slv_wire3[31:24] = 8'b0;
    assign slv_wire3[15:5] = 11'b0;
  
    
   //  slv_wire3[11:0],//sin_out,
    // slv_wire3[27:16]//cos_out
    MTF_coder coder(
        .letter(slv_reg1[7:0]),
        .key(slv_wire3[4:0]),
        .start(code_start),
        .done(slv_wire2[0]),
        .clk(clk),
        .rst(rst)
    );
    
    MTF_decoder decoder(
        .number(slv_reg1[20:16]),
        .key(slv_wire3[23:16]),
        .start(decode_start),
        .done(slv_wire2[1]),
        .clk(clk),
        .rst(rst)
    );
    
    initial begin
      clk = 0;
      rst = 0;
    end
    
    always #10 clk = ~clk;
    
    initial begin
//        decode_done = 0;
      #5 slv_reg1 = "T" ;
       slv_reg0 = 8'hAA;
      #10 slv_reg0 =0;
      
      #10 slv_reg1 = "E";
       slv_reg0 = 8'hAA;
      #10 slv_reg0 =0;
      
      #10 slv_reg1 = "E";
       slv_reg0 = 8'hAA;
      #10 slv_reg0 =0;
      
      #10 slv_reg1 = "S";
       slv_reg0 = 8'hAA;
      #10 slv_reg0 =0;
      
      #10 slv_reg1 = "S";
       slv_reg0 = 8'hAA;
      #10 slv_reg0 =0;
        
      #10 slv_reg1 = "T";
       slv_reg0 = 8'hAA;
      #10 slv_reg0 =0;
      
     #10 slv_reg1 = 0;
     
      #700 slv_reg1[20:16] = 20;
       slv_reg0 = 8'h55;
      #10 slv_reg0 =0;
      
      #10 slv_reg1[20:16] = 5;
       slv_reg0 = 8'h55;
      #10 slv_reg0 =0;
      
      #10 slv_reg1[20:16] = 1;
       slv_reg0 = 8'h55;
      #10 slv_reg0 =0;
      
      #10 slv_reg1[20:16] = 20;
       slv_reg0 = 8'h55;
      #10 slv_reg0 =0;
      
      #10 slv_reg1[20:16] = 20;
       slv_reg0 = 8'h55;
      #10 slv_reg0 =0;
        
      #10 slv_reg1[20:16] = 3;
       slv_reg0 = 8'h55;
      #10 slv_reg0 =0;
      
     slv_reg1 = 0;
     
//      #2000 $finish;
    end
    
    integer i=0;
always @(posedge clk) begin
    if(slv_wire2[0] && slv_reg3[15:0] != 30 && slv_reg3[15:0] != 0) begin
        $display("coded: = %d", slv_reg3[15:0]);
    end
    if(slv_wire2[1]) begin
        #2 $display("decoded: = %c", slv_reg3[31:16]);
        i = i+1;
        if(i == 6) $finish;
    end
end
    
endmodule
