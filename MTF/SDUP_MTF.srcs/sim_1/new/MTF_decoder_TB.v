`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 27.08.2021 17:48:55
// Design Name: 
// Module Name: MTF_decoder_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MTF_decoder_TB(

    );
    
    wire [ 4 : 0 ] key;
    reg [ 7 : 0 ] number;
    reg clk, rst;
        
    MTF_decoder decoder_test( number, key, clk, rst );
    
    initial begin
      clk = 0;
      rst = 0;
    end
    
    always #10 clk = ~clk;
    
    initial begin
      #5  number = 20;
      #20 number = 02;
      #20 number = 11;
      #20 number = 15;
      #20 number = 03;
      #20 number = 23;
      #20 number = 13;
      #20 number = 03;
      #20 number = 09;
      #20 number = 17;
      #20 number = 17;
      #20 number = 02;
      #20 number = 21;
      #20 number = 12;
    end
    
    initial begin
      #800 $finish;
    end
    
endmodule
