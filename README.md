# Algorytm - wzór, cel.

MTF (Move To Front) - Funkcja transformująca MTF zaprojektowana została w celu poprawy entropii szyfrowania technik kompresji.


Celem projektu jest wykonanie i implementacja na dostarczonej płytce modułu odpowiadającego funkcjonalności kodera i dekodera MTF.


#### Działanie MTF:
  - KODOWANIE
	- zastąpienie każdego symbolu wiadomości poprzez jego numer indeksu na liście ostatnio używanych
	- przeniesienie tego symbolu na początek tej listy
  - DEKODOWANIE
	- zastąpienie każdej liczby zaszyfrowanej wiadomości poprzez znak odpowiadający tej wartości numerem indeksu na liście ostatnio używanych
	- przeniesienie tego symbolu na początek tej listy
	
	
# Proponowana implementacja:
	
### ver. 1. -  Implementacja tabeli znaków przy pomocy struktury linked list.
Zapisanie tabeli w pamięci pod postacią strukury { [znak] , [adres następnego znaku] }
Przeszukując kolejne elementy listy zapamiętujemy poprzedni i aktulany rekord.
Jeśli aktualna pozycja zgadza się z poszukiwanym znakiem:
- W poprzednim elemencie zmieniamy adres docelowy na następny element
- W obecnym elemencie zmieniamy adres docelowy na pierwszy elemet
- Adres pierwszego elementu zmieniamy na adres obecnego elementu


### ver. 2. -  Implementacja tabeli znaków poprzez zapisanie jej w rzejestrze.
Zapisanie tabeli na { [ilość znaków w tabeli] * [ilość bitów dla znaku] } bitowym rejestrze.
Przeszukując kolejne grupy po [ilość bitów dla znaku] bitów zapamiętujemy numer indeksu aktualnego znaku.
Jeśli aktualna pozycja zgadza się z poszukiwanym znakiem:
- Przetwarzamy wartości w rejestrze w podany sposób:
    - Przed:	{ [bity znaków przed obecnym] , [obecny znak] , [bity późniejszych znaków] }
    - Po: 	{ [obecny znak] , [bity znaków przed obecnym] , [bity późniejszych znaków] }



#### Dodatkowy komentarz do wstępu:
Wydaje mi się, że ver. 1. będzie bardziej odpowiedniado implementacji algorytmu kodowania, a ver. 2. - dekodowania.
Jednak ostateczna decyzja zostanie podjęta po przetestowaniu obu metod i ewentualnych innych koncepcji.
